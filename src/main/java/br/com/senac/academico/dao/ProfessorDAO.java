package br.com.senac.academico.dao;

import br.com.senac.academico.Professor;
import java.util.List;
import javax.persistence.Query;

public class ProfessorDAO extends DAO<Professor> {

    public ProfessorDAO() {
        super(Professor.class);
    }

    public List<Professor> findByFiltro(Long id, String nome, String cpf) {
        StringBuilder sb = new StringBuilder("from Professor p where 1 = 1 ");

        if (id != null && id != 0) {
            sb.append(" AND p.id = :id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sb.append(" AND p.nome LIKE :nome ");
        }

        if (cpf != null && !cpf.isEmpty()) {
            sb.append(" AND p.cpf = :cpf ");
        }

        this.em = JPAUtil.getEntityManager();
        Query query = em.createQuery(sb.toString());

        if (id != null && id != 0) {
            query.setParameter("id", id);
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("nome", nome + "%");
        }

        if (cpf != null && !cpf.isEmpty()) {
            query.setParameter("cpf", cpf);
        }

        List<Professor> lista = query.getResultList();
        em.close();

        return lista;

    }

}
