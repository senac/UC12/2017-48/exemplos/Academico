package br.com.senac.academico.bean;

import br.com.senac.academico.Professor;
import br.com.senac.academico.dao.ProfessorDAO;
import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;


@Named(value = "professorBean")
@RequestScoped
public class ProfessorBean {

    private Professor professor;
    private ProfessorDAO dao;

    public ProfessorBean() {
        this.professor = new Professor();
        this.dao = new ProfessorDAO();
    }

    public void salvar() {

        if (this.professor.getId() == 0) {
            dao.save(professor);
        } else {
            dao.update(professor);
        }

    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
    
    public String getTexto(){
        if(this.professor!= null && this.professor.getNome() == null ){
            return "Vazio" ; 
        }else{
            return this.professor.getNome();
        }
    }
    
    public String getCodigo(){
        if(this.professor != null && this.professor.getId() != 0 ){
            return String.valueOf(this.professor.getId());
        }
        
        return "" ; 
    }
    
    
    public void novo(){
        
        this.professor = new Professor() ; 
        
    }
    
    
    

}
