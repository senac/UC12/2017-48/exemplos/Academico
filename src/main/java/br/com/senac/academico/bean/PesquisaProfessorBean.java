package br.com.senac.academico.bean;

import br.com.senac.academico.Professor;
import br.com.senac.academico.dao.ProfessorDAO;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

@Named(value = "pesquisaProfessorBean")
@RequestScoped
public class PesquisaProfessorBean {

    private Long codigo;
    private String nome;
    private String cpf;

    private List<Professor> lista;

    private ProfessorDAO dao;

    public PesquisaProfessorBean() {
    }

    @PostConstruct
    private void inicializar() {
        this.dao = new ProfessorDAO();
        this.lista = dao.findAll();

    }

    public List<Professor> getLista() {
        return lista;
    }

    public void setLista(List<Professor> lista) {
        this.lista = lista;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void pesquisar() {
        
        this.lista = dao.findByFiltro(codigo, nome, cpf);
        

    }

}
